(defproject smsauth "0.1"
  :description "2-Factor Auth using Twilio"
  :dependencies
    [[clj-http "0.2.1"]
     [com.googlecode.libphonenumber/libphonenumber "3.9"]
     [compojure "0.6.5"]
     [hiccup "0.3.6"]
     [org.clojure/clojure "1.2.1"]
     [org.clojure/clojure-contrib "1.2.0"]
     [org.clojure/java.jdbc "0.1.1"]
     [org.mindrot/jbcrypt "0.3m"]
     [postgresql/postgresql "9.1-901.jdbc4"]
     [clj-base64 "0.0.0-SNAPSHOT"]
     [ring-basic-authentication "0.0.1"]
     [ring/ring-jetty-adapter "1.0.0-RC1"]
     [sandbar/sandbar "0.4.0-SNAPSHOT"]]
  :repositories {"sonatype-oss-public" "https://oss.sonatype.org/content/groups/public"})

