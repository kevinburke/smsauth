import requests
import json
import random
import string

api_root = "http://localhost:5000/api"
service_name = "".join([random.choice(string.lowercase) for _ in xrange(8)])
password = "mypass"

def do_a_post(url, auth, data):
    return requests.post(url, auth=auth, data=data)

#print "GET /print"
#content = requests.get("http://localhost:5000/print")

#print "\n\n"

#print "DELETE /api/services"
#delete_resp = requests.delete("http://localhost:5000/api/services",
                                #auth=(service_name, password))

#print delete_resp
#if delete_resp.status_code == 204:
    #print "Deleted." + "\n\n"


data = {
    "service_name": service_name,
    "password": password
}

def register(service_name, password):
    print "Registering service..."
    print "POST /api/services"
    response = requests.post("http://localhost:5000/api/services",
                             data=data, headers={"Content-Type": "application/x-www-form-urlencoded"})
    print response.content
    resp_json = json.loads(response.content)
    token = resp_json['token']
    return token

#print "GET /print"
#content = requests.get("http://localhost:5000/print")
#print content.content

token = register(service_name, password)
#print "Checking whether service exists..."
print "GET /api/services"
init = requests.get(api_root + "/services",
                    auth=(service_name, password),
                   headers={"Content-Type": "application/x-www-form-urlencoded"})
if init.status_code == 401:
    print "401."
else:
    print init.status_code
    print init.content
    init_json = json.loads(init.content)
    token = init_json['token']

data = {
    "phone_number": "925-271-7005"
}

response = requests.post("/".join([api_root, service_name, "phone"]),
                         auth=(service_name, token),
                         data=data)

print response.content
print response.status_code
assert response.status_code == 201

response = requests.post("/".join([api_root, service_name, "phone",
                                   "authorize"]),
                         auth=(service_name, token),
                         data=data)

assert response.status_code == 204
print response.content

print "what is the code?"
code = raw_input()

data['validation_code'] = code

response = requests.post("/".join([api_root, service_name, "phone",
                                   "verify"]),
                         auth=(service_name, token),
                         data=data)

print response
assert response.status_code == 200
print response.content

# test rate limiting

for i in xrange(6):
    response = requests.post("/".join([api_root, service_name, "phone",
                                   "authorize"]),
                         auth=(service_name, token),
                         data=data)
    if i is 6:
        assert response.status_code == 503

#if token:
    #print "The token is " + token

#def print_things(args):
    #print args

#print "Trying to authorize phone on service..."
#request = requests.post("http://localhost:5000/api/" + service_name + "/phone/authorize",
                        #auth=(service_name, token),
                        #hooks=dict(args=print_things))

#print request
#print request.status_code
