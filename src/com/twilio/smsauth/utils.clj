(ns com.twilio.smsauth.utils
  "Utility functions."
  (:require [remvee.base64 :as base64])
  (:import [com.google.i18n.phonenumbers PhoneNumberUtil]
           [com.google.i18n.phonenumbers NumberParseException]
           [org.mindrot.jbcrypt BCrypt]))

(defn remove-base-64
  "Turn a base64 encoded string into plain text."
  [encoded-str]
  (let [without-basic (re-find #"[^ ]+$" encoded-str)]
    (base64/decode-str without-basic)))

(defn get-auth-header
  "return authorization header values or nil"
  [auth-type auth-header]
  (if auth-header
    (let [raw-auth (remove-base-64 auth-header)
          ; XXX combine these
          service-name (re-find #"^[^:]+" raw-auth)
          password (re-find #"[^:]+$" raw-auth)]
      {:service-name service-name, auth-type password})
    ; else there's no auth header
    nil))

(defn compare-fn
  "Compare two tokens for equality, or a raw password and a salted one."
  [auth-type in-request in-db]
(do
  (println auth-type in-request in-db)
  (cond (= auth-type :password)
        (try
          (BCrypt/checkpw in-request in-db)
          (catch NullPointerException e
            false))
        (= auth-type :token)
        (= in-request in-db))))

(defn parse-phone-no
  "Convert the phone number to standard form, using the libphonenumber class.
  Arguments:
    raw-phone-no - the phone number to convert
  Returns:
    the canonical version of the phone number, or nil if the phone number was 
    invalid."
  [raw-phone-no]
  (let [phone-util (PhoneNumberUtil/getInstance)]
    (try
      (let [us-number (.parse phone-util raw-phone-no "US")]
        (.getNationalNumber us-number))
      (catch NumberParseException e
        nil))))

(defn generate-token 
  "Generate a random token."
  []
  (.replaceAll (.toString (java.util.UUID/randomUUID)) "-" ""))

(defn stringify-and-pad
  "Convert a number to a string, and pad to the specified number of digits."
  [number num-digits]
  (let [str-num-digits (Integer/toString num-digits)]
    (format (str "%0" str-num-digits "d") number)))

(defn rand-six-digit-number
  "Generate a random six digit number."
  []
  (let [random (java.util.Random.)]
    (.nextInt random 1000000)))

(comment
  Password hashing methods)

(defn gen-salt
  "Generate a password salt."
  ; This is actually two functions with the same name. If we pass an argument
  ; to gen-salt, then generate a salt with the size. Otherwise use the default
  ; gensalt parameter.
  ([size]
   (BCrypt/gensalt size))
  ([]
   (BCrypt/gensalt)))

(defn hash-pass 
  "Create a hash of a password before storing it in the DB." 
  ([salt raw] (BCrypt/hashpw raw salt))
  ([raw] (hash-pass (gen-salt) raw)))

(defn compare-pass
  "Compare a raw pass with an already hashed password"
  [raw encrypted]
  (BCrypt/checkpw raw encrypted))

