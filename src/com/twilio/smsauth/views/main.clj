(ns com.twilio.smsauth.views.shouts
  (:use [hiccup.core :only [html h]]
        [hiccup.page-helpers :only [doctype]]
        [hiccup.form-helpers :only [form-to label text-area submit-button]])
  (:require [com.twilio.smsauth.views.layout :as layout]))

(defn register [service-name token]
  [:div {:id "container"}
    [:h2 service-name]
    [:h2 token]
  ]
)
