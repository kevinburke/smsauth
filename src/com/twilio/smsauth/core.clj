(ns com.twilio.smsauth.core
  (:use [compojure.core :only [defroutes]]
        [remvee.ring.middleware.basic-authentication]
        [hiccup.page-helpers :only (html5)])
  (:require [compojure.route :as route] 
            [compojure.handler :as handler]
            [ring.adapter.jetty :as ring]
            [remvee.ring.middleware.basic-authentication :as auth]
            [com.twilio.smsauth.controllers.main :as controller]
            [com.twilio.smsauth.models.main :as model]
            [com.twilio.smsauth.views.layout :as layout]
            [com.twilio.smsauth.utils :as utils]))

(defn authenticated?
  "authenticate the request"
  [service-name token]
  (do
    (println "authenticating service: " service-name " and token: " token)
    (:valid (model/valid-service-and-token service-name token))))

(defn password-authenticated?
  "Authenticate the given service name and password"
  [service-name pass]
  (do
    (println (str "authenticating service: " service-name " and password: " pass))
    (def service-info-or-nil (model/get-service-and-pass service-name))
    (println service-info-or-nil)
    (if service-info-or-nil
        (do
          (println "Comparing pass to stored pass")
          (utils/compare-pass pass (:password service-info-or-nil)))
        (do
          (println "Service " service-name " not in the database, serving 401")
          false))))

(defroutes public-routes
  ; match anything defined in com.twilio.smsauth.controllers.main/routes
  controller/routes

  ; match anything in the static dir at resources/public
  (route/resources "/"))

           ;controller/token-routes
           ;controller/password-routes)
 ;(auth/wrap-basic-authentication 
             ;controller/token-routes authenticated?))

(defn wrap-dir-index [handler]
  (fn [req]
    (handler
      (update-in req [:uri]
        #(if (= "/" %) "/index.html" %)))))

(def application (-> (handler/site public-routes)
    (wrap-dir-index)))

(defn start [port]
  (ring/run-jetty (var application) {:port (or port 8000) :join? false}))

(defn -main []
  (let [port (Integer/parseInt (System/getenv "PORT"))]
    (start port)))
