(use 'clojure.contrib.http.agent)

(defn handler
  "handle the stream"
  [response]
  (do
    (def resp (slurp response))
    (println resp)))

(http-agent "http://jsonip.com"
              :handler handler)
