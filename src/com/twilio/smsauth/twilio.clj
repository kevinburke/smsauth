(ns com.twilio.smsauth.twilio
  "Code to interface with Twilio libraries"
  (:require [clojure.contrib.base64 :as base64]
            [clj-http.client :as client]
            [clojure.contrib.json :as json]))

(def sms-url (str "https://api.twilio.com/2010-04-01/Accounts/"
                  (System/getenv "TWILIO_ACCOUNT_SID")
                  "/SMS/Messages.json"))

; would rather programatically discover this, but it'll work for now.
(def test-sms-url "http://localhost:5000/test/sms/send")

(def real-url sms-url)

(defn send-sms
  "Send an SMS to Twilio."
  [url phone-no message]
  (do
    (def post-body {:From "+19255218784",
                    :To phone-no,
                    :Body message})
    ; return the response
    (client/post url
                  {:basic-auth [(System/getenv "TWILIO_ACCOUNT_SID") 
                                  (System/getenv "TWILIO_AUTH_TOKEN")]
                   :form-params post-body})))

(defn send-sms-verify
  "Send an SMS to Twilio.
   Arguments:
      url: the URL to post the request to
      phone-no: the phone number to send the message to
      service-name: the service attached to the phone number
   Returns:
      A string containing the formatted phone number."
  [url phone-no service-name]
  (do
    (def message (str "This number has been successfully registered for "
                      "SMSAuth with "
                       service-name ". To cancel, reply STOP."))
    (def twilio-response (send-sms url phone-no message))
    (def response (json/read-json (twilio-response :body)))
    (response :to)))
