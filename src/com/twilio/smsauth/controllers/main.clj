(ns com.twilio.smsauth.controllers.main
  "Route controller for the app."
  (:use     [compojure.core :only [defroutes GET POST DELETE]]
            [hiccup.core :only [html h]]
            [remvee.ring.middleware.basic-authentication]
            [clojure.contrib.str-utils])
  (:require [clojure.string :as str]
            [clojure.contrib.json :as json]
            [clojure.contrib.math :only expt]
            [com.twilio.smsauth.utils :as utils]
            [com.twilio.smsauth.constants :as constants]
            [com.twilio.smsauth.models.main :as model]
            [com.twilio.smsauth.twilio :as twilio])
  (:import  [com.google.i18n.phonenumbers PhoneNumberUtil]
            [com.google.i18n.phonenumbers NumberParseException]))

(defn delete-service
  "Delete the given service from the DB."
  [request]
  (println "Url requested is " (:request-method request) " " (:uri request))
  (let [params (:params request)
        service-name (:service_name params)
        password (:password params)]
      (if (model/service-in-db service-name)
          (do
            (model/delete-service service-name)
            {:status 204})

          {:status 400,
           :body (json/json-str {:error "There is no service 
                                        with the given name."})})))

(defn register
  "Register a new service.
   Arguments:
     params: a map containing:
       service_name - the name of the service
       pass - the account password
   Returns:
     201 resource created - with a custom auth token
     409 conflict - resource already exists
     400 bad request - didn't include one of the parameters"
  [request]
  (println "Url requested is " (:request-method request) " " (:uri request))
  (println request)
  (let [params (:params request)
        service-name (:service_name params)
        password (:password params)]
    ; If request doesn't have the right parameters, return 400
    (cond (or (= nil service-name) (= nil password) 
              (< (count password) 5) (< (count service-name) 3)
              (> (count service-name) 20) (> (count password) 100))
          {:status 400
          :headers {"Content-Type" "text/plain"}
          :body "Your request was missing a service_name or a password. The
  service name must be between 3 and 20 characters, and the password must be
  between 5 and 100 characters."}
        ; Else try to select from the DB. If there's already a record with the
        ; same service name, return 409
        (:valid (model/service-in-db service-name))
        {:status 409
         :headers {"Content-Type" "text/plain"}
         :body (json/json-str {:error "A service already exists with that name. Please try again."})}
    ; Else create the resource, store the password hash, create a token,
    ; and then return a 201
      :else 
      (let
        [token (utils/generate-token)
         pass-hash (utils/hash-pass password)]
        (model/store-service-in-db :services service-name pass-hash token)
        {:status 201
         ; XXX send a better response body
         :body (json/json-str {:service_name service-name, 
                               :token token}) }))))

(defn add-phone
  "Add a phone number to a given service. There are a few steps we have to take:
    - Validate the service name and token
    - Validate the phone number 
    - Send a text message to the number
    - Store the number in the database
  Arguments:
    request - an authorized HTTP request
  Returns:
    201 Resource Created - phone number added to a service
    400 Bad Request - invalid phone number or bad request"
  [request auth-type]
  (let [auth-header ((:headers request) "authorization")
        auth-map (utils/get-auth-header auth-type auth-header)]
    (if (model/invalid-user-and-pass auth-map auth-type)
        {:status 401,
         :headers {"WWW-Authenticate" "Basic realm=\"smsauth.herokuapp.com\""}}
    (let [raw-phone-no (:phone_number (:params request))
          valid-phone-no (utils/parse-phone-no raw-phone-no)
          service-name (:service-name auth-map)]
      (if (not valid-phone-no)
        {:status 400,
          :body (json/json-str {:error "Please include a valid phone_number."})}
        ; XXX get the service id some other way.
        (let [service-map (model/service-in-db service-name)
              service-id (service-map :id)]
          ; XXX check that service doesn't already have phone number
          (model/store-phone-no :numbers service-id valid-phone-no)
          (twilio/send-sms-verify twilio/real-url valid-phone-no service-name)
          ; XXX jsonify this, send more information, document it.
          {:status 201,
           :body (json/json-str {
                    :service_name service-name,
                    :phone_number valid-phone-no,
                    :status "active",
                    :uri (:uri request)
                    ; should have some kind of unique id for the phone number
                    ; here.
                    ; also return the date
                  })}))))))

(defn handle-incoming-sms
  "Handle incoming SMS messages (probably number validation)"
  [request]
  ; check that the request is yes
  ; if so get the phone number
  ; mark the phone number as validated
  (println "Url requested is " (:request-method request) " " (:uri request))
  (println request)
  (let [params (request :params)
        body (params :Body)
        raw-phone-no (params :From)
        canon-phone-no (utils/parse-phone-no raw-phone-no)]
    (cond (= body "STOP")
          (do
            ; XXX send back TwiML with a "unsubscribed" message
            ; unsubscribe the user
            (model/mark-as-validated :numbers canon-phone-no)
            {:status 204})
          :else
          {:status 400
           :body "We couldn't understand your request."})))

; XXX move this to a test class
(defn handle-test-sms
  "Send a fake response to a test message."
  [request]
  (println "Test SMS! " request)
  {:status 201
   :body (json/json-str
            {:sid "test", 
             :account_sid "testacctsid",
             :to "+19252717005",
             :from "+19255218784",
             :body "A Test Message"})})

(defn authorize
  "Authorize the request."
  [request auth-type]
  (let 
    [auth-header ((:headers request) "authorization")
     auth-map (utils/get-auth-header auth-type auth-header)]
    (if (model/invalid-user-and-pass auth-map auth-type)
        {:status 401,
         :headers {"WWW-Authenticate" "Basic realm=\"smsauth.herokuapp.com\""}}
      (let [params (:params request)
            service-name (:service-name auth-map)
            token (:token auth-map)
            raw-phone-no (:phone_number params)
            canon-phone-no (utils/parse-phone-no raw-phone-no)
            service-map (model/valid-service-and-token service-name token)
            phone-map (model/get-phone-map (:id service-map) canon-phone-no)]
    (cond
      ; check that service has phone number
      (not (model/service-has-phone (:id service-map) canon-phone-no))
      {:status 400,
       :body (json/json-str 
              {:error "That service has not registered the phone number."})}
      ; if phone number has been validated
      (not (model/phone-no-is-validated (:id service-map) canon-phone-no))
      {:status 400,
       :body (json/json-str 
              {:error "The user has not authorized your service to receive 
             authorization messages"})}
      (not canon-phone-no)
      {:status 400,
       :body "Please provide a valid US phone number."}
      (not (:valid service-map))
      {:status 403,
       :body (json/json-str 
              {:error "Please provide a valid service and authorization token."})}
      ; if service hasn't been hitting us for too many requests
      (model/service-over-rate-limit (:id service-map) (:id phone-map) 
        constants/num-requests constants/seconds)
      {:status 503,
       :body (json/json-str 
                {:error "Sorry, you are currently over the rate limit. 
                       Try again in a few minutes."})}

    ; generate a random number, store number and creation date in the DB, text
    ; message the user, and then give the number in the response body.
    :else 
    ; XXX wrap this all in a try/catch.
    (let [ 
    ; XXX this line should be rewritten
          the-random-number (utils/stringify-and-pad 
                                (utils/rand-six-digit-number) 6)
          message (str "Your SMSAuth verification code is " 
                        the-random-number ". To cancel reply STOP")]
      (twilio/send-sms twilio/real-url canon-phone-no message)
      (model/store-random-number service-map canon-phone-no the-random-number)
      {:status 204}))))))

(defn status-and-response
  "Return a response with the given status and response."
  [status type-of-response response]
  {:status status, 
   :body (json/json-str {type-of-response response})})

(defn verify
  "Verify a given auth code.
  Ensure a valid service name and token, 
  ensure a valid phone number,
  fetch all numbers issued for the given phone number in the last ten minutes,
  check that the supplied code matches one of the database validation codes.
  Arguments:
    request - an authorized HTTP request
  Returns:
    200 OK - if the code is valid
    400 bad request - invalid service name or token, invalid phone number, 
                      unauthorized phone number, bad validation code
    503 service unavailable - too many requests lately"
  [request auth-type]
  (println "Url requested is " (:request-method request) " " (:uri request))
  (println request)
  (let 
    [auth-header ((:headers request) "authorization")
     auth-map (utils/get-auth-header auth-type auth-header)]
    (if (model/invalid-user-and-pass auth-map auth-type)
        {:status 401,
         :headers {"WWW-Authenticate" "Basic realm=\"smsauth.herokuapp.com\""}}
      (let [params (:params request)
            service-name (:service-name auth-map)
            token (:token auth-map)
            raw-phone-no (:phone_number params)
            canon-phone-no (utils/parse-phone-no raw-phone-no)
            validation-code (:validation_code params)
            service-map (model/valid-service-and-token service-name token)
            phone-map (model/get-phone-map (:id service-map) canon-phone-no)]
        (cond
          (not canon-phone-no)
          (status-and-response 400 :error 
              "Please provide a valid US phone number.")
          (not (:valid service-map))
          (status-and-response 403 :error 
                  "Please provide a valid service and authorization token.")
          (not (model/service-has-phone (:id service-map) canon-phone-no))
          (status-and-response 400 :error 
              "The phone number provided is not registered for the given service.")
          (not (model/phone-no-is-validated (:id service-map) canon-phone-no))
          (status-and-response 400 :error 
              "The user has not authorized your service to receive text messages.")
          (not (model/valid-code canon-phone-no validation-code))
          (status-and-response 400 :error 
              "The validation code provided was invalid.")
          (model/service-over-rate-limit (:id service-map) (:id phone-map) 
            constants/num-requests constants/seconds)
          (status-and-response 503 :error 
              "Sorry, you are currently over the rate limit. Try again in a few minutes.")
          :else
          ; wipe the validation code from the DB
          ; return 200 to user
          (do
            (model/wipe-from-db canon-phone-no validation-code)
            {:status 200,
             :body (json/json-str {:code "valid",
                                   :service_name service-name,
                                   :uri (:uri request),
                                   :phone_number canon-phone-no,
                                   :validation_code validation-code})}))))))
          ; XXX should catch errors and throw a 500

(defn get-service-info
  "Return service info."
  [request auth-type]
  (let [auth-header ((:headers request) "authorization")
        auth-map (utils/get-auth-header auth-type auth-header)]
    (if (model/invalid-user-and-pass auth-map auth-type)
        {:status 401,
         :headers {"WWW-Authenticate" "Basic realm=\"smsauth.herokuapp.com\""}}
    (let [service-name (:service-name auth-map)
          service-map (model/service-in-db service-name)]
          {:status 200,
           :body (json/json-str {:service_name service-name, 
                                 :token (:token service-map)})}))))

(defn print-db 
  "print the db"
  [request]
  (do
    (println "printing the database")
    (println (model/print-db))
    "hello world"))

(defroutes routes
  (GET "/print" request (print-db request))
  (GET "/api/services" request (get-service-info request :password))
  (POST "/api/services"  request  (register request))
  (POST "/api/sms"       request  (handle-incoming-sms request))
  (POST "/test/sms/send" request  (handle-test-sms request))

  (POST "/api/:service-name/phone" request (add-phone request :token))
  (POST "/api/:service-name/phone/verify" request (verify request :token))
  (POST "/api/:service-name/phone/authorize" request (authorize request :token))

  (DELETE "/api/services" request (delete-service request)))

;(defroutes routes
  ;(GET "/print" request (print-db request))
  ;(POST "/api/services"  request  (register request))
  ;(POST "/api/sms"       request  (handle-incoming-sms request))
  ;(POST "/test/sms/send" request  (handle-test-sms request)))

;(defroutes token-routes
  ;(POST "/api/:service-name/phone" request (add-phone request))
  ;(POST "/api/:service-name/phone/verify" request (verify request))
  ;(POST "/api/:service-name/phone/authorize" request (authorize request)))

;(defroutes password-routes
  ;(GET "/api/services" request (get-service-info request))
  ;(DELETE "/api/services" request (delete-service request)))
