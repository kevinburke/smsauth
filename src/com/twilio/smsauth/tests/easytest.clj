(ns clojure.test.example
  (:use clojure.test))

(deftest add-1-to-1
  (is (= 2 (+ 1 1))))

(run-all-tests)
