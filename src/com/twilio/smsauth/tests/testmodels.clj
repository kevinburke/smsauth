(ns shouter.tests.testmodels
  "Testing shouter.models.shout.clj.

   To run type lein test shouter.tests.testmodels"
  (:use [shouter.models.base]
        [shouter.models.migration]
        [clojure.test])
  (:require [clojure.java.jdbc :as sql]
            [shouter.twilio :as twilio]
            [shouter.models.shout :as model]))

(defn my-fixture [f]
  (sql/with-connection db
    (try
      (apply sql/create-table :test_services services-schema)
      (apply sql/create-table :test_numbers numbers-schema)
      (f)
      (finally
        (sql/drop-table :test_services)
        (sql/drop-table :test_numbers)))))

(use-fixtures :each my-fixture)

(defn get-results
  "Get the results of running the query on the DB, as a list."
  [query]
  (sql/with-query-results results
    [query]
    (into [] results)))

(deftest test-insert
  (do
    (def before-list (get-results "select * from test_services"))
    (is (= 0 (count before-list)))
    (sql/insert-values :test_services [:service_name :pass_hash :token]
                                      ["Test Service" "aestheasuea" "eaeea"])
    (def after-list (get-results "select * from test_services"))
    (is (= 1 (count after-list)))))

(deftest test-store-service
  (do
    (model/store-service-in-db :test_services "kevin" "password" "token")
    (def test-res-list (get-results "select * from test_services"))
    (is (= 1 (count test-res-list)))))

(deftest test-update-phone
  (do
    (def service-id 1)
    (def new-phone-no "9252717005")
    (model/store-phone-no :test_numbers service-id "8")
    (def before-results (get-results "select * from test_numbers"))
    (is (= "8" ((first before-results) :phone)))
    (model/update-phone-no :test_numbers new-phone-no service-id)
    (def after-results (get-results "select * from test_numbers"))
    (is (= new-phone-no ((first after-results) :phone)))))

; XXX move this to a different test class
(deftest test-get-sms
  (do
    (def to-phone-no 
      (twilio/send-sms-verify twilio/test-sms-url "9252717005" "test-service"))
    (is (= "+19252717005" to-phone-no))))

; run ALL the tests!
(run-all-tests)
