(require '[clojure.java.jdbc :as sql])
(use '[com.twilio.smsauth.models.base :only (db sql-select)])

(defn auth-service
  "Validate that the token or password is valid."
  [auth-map auth-type]
  (let [res-list (sql-select ["select * from services where service_name = ?" 
          (:service-name auth-map)])]
    (println res-list)
    (not (utils/compare-fn auth-type (auth-type auth-map) 
                        ((auth-type corresponding-db-column) res-list)))))

(println (auth-service :token "")
