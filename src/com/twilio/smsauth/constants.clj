(ns com.twilio.smsauth.constants
  "App-wide constants")

(def num-requests 5)

(def seconds 10)
