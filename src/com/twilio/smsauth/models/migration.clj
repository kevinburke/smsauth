(comment
  Code to initialize the database.)

(ns com.twilio.smsauth.models.migration
  (:use [com.twilio.smsauth.models.base :only (db)])
  (:require [clojure.java.jdbc :as sql]))

; use apply to unwrap these
(def services-schema [[:id :serial "PRIMARY KEY"]
                      [:service_name :varchar "NOT NULL"]
                      [:pass_hash :varchar "NOT NULL"]
                      [:token :varchar "NOT NULL"]])

(def numbers-schema [[:id :serial "PRIMARY KEY"]
                     [:phone :varchar "NOT NULL"]
                     [:service_name_fk :int "NOT NULL"]
                     [:validated :int "NOT NULL"]])

(def validation-codes-schema [[:id :serial "PRIMARY KEY"]
                              [:phone :varchar "NOT NULL"]
                              [:validation_code :varchar "NOT NULL"]])

(def api-requests-schema [[:id :serial "PRIMARY KEY"]
                          [:date_created :timestamp "NOT NULL DEFAULT NOW()"]
                          [:service_name_fk :int "NOT NULL"]
                          [:phone_fk :int "NOT NULL"]
                          [:request_type :int "NOT NULL"]])

(defn create-dbs []
  (sql/with-connection db
    (sql/do-commands 
      "DROP TABLE IF EXISTS services"
      "DROP TABLE IF EXISTS validation_codes"
      "DROP TABLE IF EXISTS numbers"
      "DROP TABLE IF EXISTS api_requests"
      (apply sql/create-table-ddl :services services-schema)
      (apply sql/create-table-ddl :validation_codes validation-codes-schema)
      (apply sql/create-table-ddl :numbers numbers-schema)
      (apply sql/create-table-ddl :api_requests api-requests-schema)
      (str "CREATE INDEX service_name_fk ON api_requests (service_name_fk)")
      (str "CREATE INDEX phone_fk ON api_requests (phone_fk)")
      (str "CREATE INDEX request_type ON api_requests (request_type)"))))

(defn -main []
  (print "Migrating database...") (flush)
  (create-dbs)
  (println " done"))
