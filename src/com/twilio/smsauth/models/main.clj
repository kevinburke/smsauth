(ns com.twilio.smsauth.models.main
  "The database layer"
  (:use [com.twilio.smsauth.models.base :only (db)])
  (:require [clojure.java.jdbc :as sql]
            [com.twilio.smsauth.utils :as utils]))

(defn sql-select
  "Make an SQL query and return the results."
  [query-and-args]
  (sql/with-connection db
    (sql/with-query-results results
      query-and-args
      (into [] results))))

(comment 
  Service methods)

(defn print-db
  "return the db as a list"
  []
  (sql-select ["select * from services"]))

(defn service-in-db 
  "Check if the service already exists in the database. 
  Returns a map with boolean key valid and integer key id (-1 if not in DB)"
  [service-name] 
  (let [res-list (sql-select 
    ["select * from services where service_name=?" service-name])]
    (if (> (count res-list) 0)
      (let [first-record (first res-list)] 
        {:valid true, :id (:id first-record), :token (:token first-record)})
      ; else service not in db
      {:valid false, :id nil, :token nil})))

(defn get-service-and-pass
  "Get the service name and password. Returns nil if not in the DB"
  [service-name]
  (if (not service-name)
      nil
      (let [res-list (sql-select 
          ["select * from services where service_name=?" service-name])]
        (if (> (count res-list) 0)
            {:service service-name, :password (:pass_hash (first res-list))}
            nil))))

(defn delete-service
  "Delete the service from the DB."
  [service-name]
  (sql/with-connection db
    (sql/delete-rows :services ["service_name=?" service-name])))

(def corresponding-db-column {:password :pass_hash, :token :token})

(defn auth-service
  "Validate that the token or password is valid."
  [auth-map auth-type]
  (let [res-list (sql-select ["select * from services where service_name = ?" 
          (:service-name auth-map)])]
    (not (utils/compare-fn auth-type (auth-type auth-map) 
                    ((auth-type corresponding-db-column) (first res-list))))))
      
(defn invalid-user-and-pass
  "check that the user and pass are valid"
  [auth-map auth-type]
  (if auth-map
    (auth-service auth-map auth-type)
    true))

(defn valid-service-and-token
  "Check if the service is in the DB, and if the token matches.
  
   Arguments:
    service-name: the name of the service to look up
    token: the token supposed to be associated with the service-name
  
   Returns:
    a map with boolean key valid, and integer key id (-1 if invalid token)"
  [service-name token]
  (let [valid-service-map (service-in-db service-name)]
  (if 
    (and 
      (valid-service-map :valid)
      (= token (valid-service-map :token)))
    ; return a map
    {:valid true, :id (valid-service-map :id)}

    ; service doesn't match token
    {:valid false, :id -1})))

(defn store-service-in-db 
  "Store the values in the services database."
  [table-name service-name password token]
  (sql/with-connection db
    (sql/insert-values table-name [:service_name :pass_hash :token] 
                                  [service-name password token])))

(defn service-has-phone
  "Check that the service has the given phone number."
  [service-id phone-no]
  (let [res-list (sql-select 
        ["select * from numbers where service_name_fk=? and phone=?"
          service-id (str phone-no)])]
      (> (count res-list) 0)))

(comment
  Phone number methods)

(defn store-phone-no
  "Store the phone number in the database."
  [table-name service-id phone]
  (sql/with-connection db
    (sql/insert-values table-name [:service_name_fk :phone :validated]
                                  [service-id phone 1])))

(defn get-phone-map
  "Get info about a phone number from the DB"
  [service-id phone]
  (let [res-list (sql-select 
        ["select * from numbers where service_name_fk = ? and phone = ?" 
        service-id (str phone)])
        result (first res-list)]
        {:valid (nil? result), :id (:id result)}))

(defn mark-as-validated
  "Change the phone number to validated."
  [table-name phone]
  ; XXX this is not quite right. In the model we have a user can register for
  ; multiple services and each one would require validation. This will validate
  ; the phone number for ALL services. To fix this, you probably need to send
  ; a confirmation code with each validation message and then confirm this
  ; against the database.
  (sql/with-connection db
    (sql/update-values table-name ["phone=?" phone] {:validated 1})))

(defn phone-no-is-validated
  "Check that the user for the given phone has validated service."
  [service-id canon-phone-no]
  (let [res-list (sql-select 
        ["select * from numbers where service_name_fk=?" service-id])]
    ; shouldn't have more than one result.
    (:validated (first res-list))))

(defn valid-code
  "Check that a given validation code is actually in the DB."
  [canon-phone-no validation-code]
  (let [res-list (sql-select 
      ["select * from validation_codes where validation_code=? and phone=?" 
       validation-code (str canon-phone-no)])]
      (> (count res-list) 0)))

(defn store-random-number
  "Store the given validation code in the database."
  [service-map canon-phone-no the-random-number]
  (sql/with-connection db
    (sql/insert-values :validation_codes [:validation_code :phone] 
                                         [the-random-number (str canon-phone-no)])))

(defn wipe-from-db
  "Remove the validation code from the list of valid codes in the database."
  [canon-phone-no validation-code]
  ; XXX handle errors
  (sql/with-connection db
    (sql/delete-rows :validation_codes 
      ["validation_code=? and phone=?" validation-code (str canon-phone-no)])))

(defn service-over-rate-limit
  "Check that the service is under the rate limit.
  
  1. Pull last num-requests from the DB for the service
  2. If request is shorter than num seconds, return true
  "
  [service-id phone-id num-requests seconds]
  (let [res-list (sql-select 
        ["select * from api_requests where service_name_fk=? and phone_fk=? limit ?" 
      service-id phone-id num-requests])]
    (println res-list)
    (if (> (count res-list) 0)
      (let [current-time (.getTime (new java.util.Date))
            oldest-req-time (.getTime (:date_created (last res-list)))]
      (and (= num-requests (count res-list))
           (< (- current-time oldest-req-time) (* seconds 1000))))
      false)))

