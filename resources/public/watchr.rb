#!/usr/bin/env watchr

def printAndSend cmd
    puts cmd
    system(cmd)
end

watch('(.*)\.md') {|md| printAndSend("markdown #{md[0]} > #{md[1]}.html") }
watch('(.*)\.coffee') {|md| printAndSend("coffee -c #{md[0]} > #{md[1]}.js") }

