<link href="https://bitbucket.org/kevinburke/markdowncss/raw/tip/markdown.css" rel="stylesheet"></link>

# SMS 2-Factor Authorization API

This is an API you can use to provide 2-factor authorization to your users.
You register phone numbers with us, and when users want to log in, you POST an
authorization request to our API endpoint. We then generate a random number
and text it to the user. You can verify the user entered the correct input by
sending a further POST request.

#### Integrations

We're currently working on integrations into popular web frameworks.

#### Documentation

Four steps to get started:

1. Register a service (`POST /api/services`)
2. Register phone numbers on that service (`POST /api/{service-name}/phone`)

    Then, when the user wants to log in:

3. Send a validation code to the user's phone (`POST
   /api/{service-name}/phone/authorize`)
4. Verify the user entered code and the 2-Factor generated code are identical
   (`POST /api/{service-name}/phone/verify`)

More details can be found on each specific page of the API.

# Register a service

    POST /api/services

Include the following values in the body of your POST request:

* **service_name** - the name of your service. Must be between 3 and 20
  characters.
* **password** - the password attached to your service, used to change your
  preferences and retrieve account data. Must be at least 5 characters.

#### Possible responses

* **201 Resource Created** - Successfully registered the service. This will
  return a response which contains the following attributes:

    * **service_name** - the name of the service
    * **token** - a 32-digit auth token that you will use to sign requests.

* **400 Bad Request** - if the request did not contain parameters for 
  service_name or password, or if the service name and password did not meet
  the specified length requirements.

* **409 Conflict** - if a service with the given service_name has already been
  registered.

# Register phone numbers for a service

    POST /api/{service-name}/phone

Include the following value in the body of your POST request:

* **phone_number** - the phone number to register. Format
  with a '+' and country code, like `+19255551234` ([E.164
  format](http://en.wikipedia.org/wiki/E.164)). Also accepts non-formatted US
  numbers, like `(925) 555-1234`.

#### Possible responses

* **201 Resource Created** - Successfully registered the phone number. A text
  message will be sent to the user to confirm registration.

* **400 Bad Request** - if the service has not been registered, or the phone
  number is invalid

* **401 Authorization Required** - if the request was not authorized with a
  service name and token

* **409 Conflict** - if the phone number has already been registered

Authenticate the request using your service name and auth
token. Most HTTP libraries have functions which allow
you to do this easily. For example, in Python with the
[requests](http://docs.python-requests.org/en/latest/index.html) library:

    import requests

    service_name = "example"
    service_token = "32b063cb637daa388e722cd223f6056b1"
    phone_data = {
        "phone_number": "+12125551234"
    }
    requests.post("https://smsauth.herokuapp.com/api/example/phone",
                  auth=(service_name, service_token), 
                  data=phone_data)

# Send a validation code

To send a validation code to a user's phone, make the following request:

    POST /api/{service-name}/phone/authorize

Include the following values in your authenticated POST request:

* **phone_number** - the phone number to send the validation code
  to. Format with a '+' and country code, like `+19255551234` ([E.164
  format](http://en.wikipedia.org/wiki/E.164)). Also accepts non-formatted US
  numbers, like `(925) 555-1234`.

#### Possible responses

* **204 No Content** - Generate a validation code and text message it to the
  user. When the user enters the input, you can verify the validation code
  matches by posting to `/api/{service-name}/phone/verify`.

* **400 Bad Request** - if the phone number is not registered for the given
  service, or the phone number provided is not valid.

* **401 Authorization Required** - if the request was not authorized with a
    service name and token.

* **403 Forbidden** - if the service name or token is invalid.

#### Code Sample

Authenticate the request using your service name and auth
token. Most HTTP libraries have functions which allow
you to do this easily. For example, in Python with the
[requests](http://docs.python-requests.org/en/latest/index.html) library:

    import requests

    api_url = "https://smsauth.herokuapp.com"
    service_name = "example"
    service_token = "32b063cb637daa388e722cd223f6056b1"
    phone_data = {
        "phone_number": "+12125551234"
    }
    response = requests.post(api_url + "/api/example/phone/authorize",
                             auth=(service_name, service_token), 
                             data=phone_data)
    print response.content

#### Note

Each authorization request will be valid for only 10 minutes. You will not be
able to generate more than 5 authorization tokens in a 10 minute period.

# Verify a user-submitted code

To verify that a code submitted by a user on your site matches the code
sent via text message to the user, send the following authenticated request:

    POST /api/{service-name}/phone/verify

Include the following values with your post request:

* **phone_number** - the phone number to send the validation code
  to. Format with a '+' and country code, like `+19255551234` ([E.164
  format](http://en.wikipedia.org/wiki/E.164)). Also accepts non-formatted US
  numbers, like `(925) 555-1234`.

* **validation_code** - the validation code sent by the user.

#### Possible Responses

* **200 OK** - The validation code provided by the request matched a code sent
  to the phone number provided by the request within the last 10 minutes.

* **400 Bad Request** - One of the following conditions did not match:

    * The request did not contain a valid phone number or validation code

    * The phone number is not authorized for the given service

    * The validation code provided did not match any of the codes sent to
          the user in the last 10 minutes

* **401 Authorization Required** - if the request was not authorized with a
  service name and token.

* **503 Service Unavailable** - if you've exceeded your rate limit by issuing
  too many requests. Try your request again later.

#### Note

To prohibit dictionary attacks, you will be limited to ten attempts per phone
number in any ten-minute period.

#### Code Sample

Authenticate the request using your service name and auth
token. Most HTTP libraries have functions which allow
you to do this easily. For example, in Python with the
[requests](http://docs.python-requests.org/en/latest/index.html) library:

    import requests

    api_url = "https://smsauth.herokuapp.com"
    service_name = "example"
    service_token = "32b063cb637daa388e722cd223f6056b1"
    # Let's say you just received a verification code of 555555
    verification_code = "555555"
    phone_data = {
        "phone_number": "+12125551234",
        "verification_code": verification_code
    }
    response = requests.post(api_url + "/api/example/phone/verify",
                             auth=(service_name, service_token), 
                             data=phone_data)
    print response.content

<script src="//yandex.st/highlightjs/6.1/highlight.min.js"></script>
<link href="/zenburn.css" rel="stylesheet" />
<script>hljs.initHighlightingOnLoad();</script>

