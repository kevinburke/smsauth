<p><link href="https://bitbucket.org/kevinburke/markdowncss/raw/tip/markdown.css" rel="stylesheet"></link></p>

<h1>SMS 2-Factor Authorization API</h1>

<p>This is an API you can use to provide 2-factor authorization to your users.
You register phone numbers with us, and when users want to log in, you POST an
authorization request to our API endpoint. We then generate a random number
and text it to the user. You can verify the user entered the correct input by
sending a further POST request.</p>

<h4>Integrations</h4>

<p>We're currently working on integrations into popular web frameworks.</p>

<h4>Documentation</h4>

<p>Four steps to get started:</p>

<ol>
<li>Register a service (<code>POST /api/services</code>)</li>
<li><p>Register phone numbers on that service (<code>POST /api/{service-name}/phone</code>)</p>

<p>Then, when the user wants to log in:</p></li>
<li><p>Send a validation code to the user's phone (<code>POST
/api/{service-name}/phone/authorize</code>)</p></li>
<li>Verify the user entered code and the 2-Factor generated code are identical
(<code>POST /api/{service-name}/phone/verify</code>)</li>
</ol>

<p>More details can be found on each specific page of the API.</p>

<h1>Register a service</h1>

<pre><code>POST /api/services
</code></pre>

<p>Include the following values in the body of your POST request:</p>

<ul>
<li><strong>service_name</strong> - the name of your service. Must be between 3 and 20
characters.</li>
<li><strong>password</strong> - the password attached to your service, used to change your
preferences and retrieve account data. Must be at least 5 characters.</li>
</ul>

<h4>Possible responses</h4>

<ul>
<li><p><strong>201 Resource Created</strong> - Successfully registered the service. This will
return a response which contains the following attributes:</p>

<ul>
<li><strong>service_name</strong> - the name of the service</li>
<li><strong>token</strong> - a 32-digit auth token that you will use to sign requests.</li>
</ul></li>
<li><p><strong>400 Bad Request</strong> - if the request did not contain parameters for 
service_name or password, or if the service name and password did not meet
the specified length requirements.</p></li>
<li><p><strong>409 Conflict</strong> - if a service with the given service_name has already been
registered.</p></li>
</ul>

<h1>Register phone numbers for a service</h1>

<pre><code>POST /api/{service-name}/phone
</code></pre>

<p>Include the following value in the body of your POST request:</p>

<ul>
<li><strong>phone_number</strong> - the phone number to register. Format
with a '+' and country code, like <code>+19255551234</code> (<a href="http://en.wikipedia.org/wiki/E.164">E.164
format</a>). Also accepts non-formatted US
numbers, like <code>(925) 555-1234</code>.</li>
</ul>

<h4>Possible responses</h4>

<ul>
<li><p><strong>201 Resource Created</strong> - Successfully registered the phone number. A text
message will be sent to the user to confirm registration.</p></li>
<li><p><strong>400 Bad Request</strong> - if the service has not been registered, or the phone
number is invalid</p></li>
<li><p><strong>401 Authorization Required</strong> - if the request was not authorized with a
service name and token</p></li>
<li><p><strong>409 Conflict</strong> - if the phone number has already been registered</p></li>
</ul>

<p>Authenticate the request using your service name and auth
token. Most HTTP libraries have functions which allow
you to do this easily. For example, in Python with the
<a href="http://docs.python-requests.org/en/latest/index.html">requests</a> library:</p>

<pre><code>import requests

service_name = "example"
service_token = "32b063cb637daa388e722cd223f6056b1"
phone_data = {
    "phone_number": "+12125551234"
}
requests.post("https://smsauth.herokuapp.com/api/example/phone",
              auth=(service_name, service_token), 
              data=phone_data)
</code></pre>

<h1>Send a validation code</h1>

<p>To send a validation code to a user's phone, make the following request:</p>

<pre><code>POST /api/{service-name}/phone/authorize
</code></pre>

<p>Include the following values in your authenticated POST request:</p>

<ul>
<li><strong>phone_number</strong> - the phone number to send the validation code
to. Format with a '+' and country code, like <code>+19255551234</code> (<a href="http://en.wikipedia.org/wiki/E.164">E.164
format</a>). Also accepts non-formatted US
numbers, like <code>(925) 555-1234</code>.</li>
</ul>

<h4>Possible responses</h4>

<ul>
<li><p><strong>204 No Content</strong> - Generate a validation code and text message it to the
user. When the user enters the input, you can verify the validation code
matches by posting to <code>/api/{service-name}/phone/verify</code>.</p></li>
<li><p><strong>400 Bad Request</strong> - if the phone number is not registered for the given
service, or the phone number provided is not valid.</p></li>
<li><p><strong>401 Authorization Required</strong> - if the request was not authorized with a
service name and token.</p></li>
<li><p><strong>403 Forbidden</strong> - if the service name or token is invalid.</p></li>
</ul>

<h4>Code Sample</h4>

<p>Authenticate the request using your service name and auth
token. Most HTTP libraries have functions which allow
you to do this easily. For example, in Python with the
<a href="http://docs.python-requests.org/en/latest/index.html">requests</a> library:</p>

<pre><code>import requests

api_url = "https://smsauth.herokuapp.com"
service_name = "example"
service_token = "32b063cb637daa388e722cd223f6056b1"
phone_data = {
    "phone_number": "+12125551234"
}
response = requests.post(api_url + "/api/example/phone/authorize",
                         auth=(service_name, service_token), 
                         data=phone_data)
print response.content
</code></pre>

<h4>Note</h4>

<p>Each authorization request will be valid for only 10 minutes. You will not be
able to generate more than 5 authorization tokens in a 10 minute period.</p>

<h1>Verify a user-submitted code</h1>

<p>To verify that a code submitted by a user on your site matches the code
sent via text message to the user, send the following authenticated request:</p>

<pre><code>POST /api/{service-name}/phone/verify
</code></pre>

<p>Include the following values with your post request:</p>

<ul>
<li><p><strong>phone_number</strong> - the phone number to send the validation code
to. Format with a '+' and country code, like <code>+19255551234</code> (<a href="http://en.wikipedia.org/wiki/E.164">E.164
format</a>). Also accepts non-formatted US
numbers, like <code>(925) 555-1234</code>.</p></li>
<li><p><strong>validation_code</strong> - the validation code sent by the user.</p></li>
</ul>

<h4>Possible Responses</h4>

<ul>
<li><p><strong>200 OK</strong> - The validation code provided by the request matched a code sent
to the phone number provided by the request within the last 10 minutes.</p></li>
<li><p><strong>400 Bad Request</strong> - One of the following conditions did not match:</p>

<ul>
<li><p>The request did not contain a valid phone number or validation code</p></li>
<li><p>The phone number is not authorized for the given service</p></li>
<li><p>The validation code provided did not match any of the codes sent to
  the user in the last 10 minutes</p></li>
</ul></li>
<li><p><strong>401 Authorization Required</strong> - if the request was not authorized with a
service name and token.</p></li>
<li><p><strong>503 Service Unavailable</strong> - if you've exceeded your rate limit by issuing
too many requests. Try your request again later.</p></li>
</ul>

<h4>Note</h4>

<p>To prohibit dictionary attacks, you will be limited to ten attempts per phone
number in any ten-minute period.</p>

<h4>Code Sample</h4>

<p>Authenticate the request using your service name and auth
token. Most HTTP libraries have functions which allow
you to do this easily. For example, in Python with the
<a href="http://docs.python-requests.org/en/latest/index.html">requests</a> library:</p>

<pre><code>import requests

api_url = "https://smsauth.herokuapp.com"
service_name = "example"
service_token = "32b063cb637daa388e722cd223f6056b1"
# Let's say you just received a verification code of 555555
verification_code = "555555"
phone_data = {
    "phone_number": "+12125551234",
    "verification_code": verification_code
}
response = requests.post(api_url + "/api/example/phone/verify",
                         auth=(service_name, service_token), 
                         data=phone_data)
print response.content
</code></pre>

<script src="//yandex.st/highlightjs/6.1/highlight.min.js"></script>

<p><link href="/zenburn.css" rel="stylesheet" /></p>

<script>hljs.initHighlightingOnLoad();</script>
