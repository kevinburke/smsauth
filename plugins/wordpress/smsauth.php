<?php
/*
Plugin Name: SMSAuth
Plugin URI: https://smsauth.herokuapp.com
Description: A 2-Factor Authentication solution for Wordpress. Don't get hacked!
Version: 1.0
Author: Kevin Burke
Author URI: http://kev.inburke.com
License: GPLv2 or later
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**
 * Add One-time Password field to login form.
 */

$smsauth_api_url = 'https://smsauth.herokuapp.com/api';
$smsauth_user_agent = 'smsauth-wordpress/1.0';

function smsauth_login_form() {
    $options = get_option('smsauth_options');
    $service_name = $options['smsauth_service_name'];
    $password = $options['smsauth_service_pass'];
    $token = $options['smsauth_service_token'];
    $phone = "925-271-7005";
    //smsauth_authorize($service_name, $token, $phone); 
    ?>
<p>
    <label for="smsauth">SMS Authorization Code</label>
    <input type="text" name="smsauth" class="input" value="" size="20" tabindex="25"/> 
</p>
<?php 
}

function smsauth_add_phone($service_name, $token, $phone) {
    global $smsauth_api_url, $smsauth_user_agent;

    $url = "$smsauth_api_url/$service_name/phone/";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, $service_name . ':' . $token);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "phone_number=$phone");
    curl_setopt($ch, CURLOPT_USERAGENT, $smsauth_user_agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
}

function smsauth_authorize($service_name, $token, $phone) {
    global $smsauth_api_url, $smsauth_user_agent;

    $url = "$smsauth_api_url/$service_name/phone/authorize";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, $service_name . ':' . $token);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "phone_number=$phone");
    curl_setopt($ch, CURLOPT_USERAGENT, $smsauth_user_agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
}

if(!function_exists('_log')){
  function _log( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
}

function smsauth_verify($service_name, $token, $phone, $code) {
    global $smsauth_api_url, $smsauth_user_agent;
    $url = "$smsauth_api_url/$service_name/phone/verify";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERPWD, $service_name . ':' . $token);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "phone_number=$phone&validation_code=$code");
    curl_setopt($ch, CURLOPT_USERAGENT, $smsauth_user_agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);
    return $info['http_code'] === '200';
}

/**
 * loginform info used in the case where PHP is missing vital functions and therefore can't use the plugin.
 */
function yubikey_loginform_functionsmissing() {
  echo "<p style=\"font-size: 12px;width: 97%;padding: 3px;\">";
  echo __('SMSAuth authentication has been disabled, Your PHP installation is missing one or more vital functions. Both the Curl & Hash functions must be present for this plugin to work.','smsauth');
  echo "</p>";
}

/**
 * loginform info used in the case where no API ID or Key has been setup.
 */
function yubikey_loginform_apiinfomissing() {
  echo "<p style=\"font-size: 12px;width: 97%;padding: 3px;\">";
  echo __('SMSAuth authentication is disabled. You need to register a SMSAuth service in your Wordpress Dashboard.','smsauth');
  echo "</p>";
}


/**
 * Optionspage for editing Yubikey global options 
 */
//function smsauth_options_page() {   
/*?>    
<div class="wrap">
    //<h2><?php _e('SMSAuth Plugin Options','smsauth');?></h2>
    //<form name="smsauth" method="post" action="options.php">
        //<?php wp_nonce_field('update-options'); ?>
        //<input type="hidden" name="action" value="update" />
        //<input type="hidden" name="page_options" value="smsauth_service_name,smsauth_service_pass" />
        //<table class="form-table">
            //<?php PHP4_Check();?>
            //<tr valign="top">
                //<th scope="row"><label for="smsauth_service_name"><?php _e('SMSAuth Service Name','smsauth');?></label></th>
                //<td><input name="smsauth_service_name" type="text" id="smsauth_service_name" class="code" value="<?php echo get_option('smsauth_service_name') ?>" size="40" /><br /></td>
            //</tr>
            //<tr valign="top">
                //<th scope="row"><label for="smsauth_service_pass"><?php _e('SMSAuth Service Password','yubikey');?></label></th>
                //<td><input name="smsauth_service_pass" type="text" id="smsauth_service_pass" class="code" value="<?php echo get_option('smsauth_service_pass'); ?>" size="40" /><br /></td>
            //</tr>
        //</table>
        //<p class="submit">
            //<input type="submit" name="Submit" value="<?php _e('Save Changes', 'smsauth' ) ?>" />
        //</p>
    //</form>
//</div>
 <?php*/
//}

/**
 * Display a warning if the PHP installation is to old.
 * To be removed later on when PHP4 is completely dead.
 */
function PHP4_Check($globaloptions=true) {
    if (version_compare(PHP_VERSION, '5.0.0', '<')){
        $errormessage=__('WARNING: You appear to be using PHP4, PHP5 or newer is required for the SMSAuth plugin to work.','smsauth');
        if ($globaloptions) {
            echo "<tr valign=\"top\">";
            echo "<th scope=\"row\">&nbsp;</th>";
            echo "<td><strong>".$errormessage."</strong></td>";
            echo "</tr>";
        } else {
            echo "<tr>";
            echo "<th>&nbsp;</th>";
            echo "<td><strong>".$errormessage."</strong></td>";
            echo "</tr>";
        }
    }
}

/**
 * Attach a Yubikey options page to the settings menu
 */
function smsauth_admin() {
    add_options_page('SMSAuth', 'SMSAuth', 8, 'smsauth', 'smsauth_options_page');
}

/**
 * Login form handling.
 * Do OTP check if user has been setup to do so.
 * @param wordpressuser
 * @return loginstatus
 */
function yubikey_check_otp($user) {
    $options = get_option('smsauth_options');
    $service_name = $options['smsauth_service_name'];
    $password = $options['smsauth_service_pass'];
    $token = $options['smsauth_service_token'];

    $code = $_POST['smsauth'];

    // send verification message to the server

    if ($yubikeyserver=='yubico') {
        // Does keyid match ?
        if (strtoupper($yubikey_key_id)!=strtoupper($keyid) && strtoupper($yubikey_key_id2)!=strtoupper($keyid) && strtoupper($yubikey_key_id3)!=strtoupper($keyid)) {
            return false;
        }
        // is OTP valid ?
        if (yubikey_verify_otp($otp,$yubico_api_id,$yubico_api_key)) {
            return $user;
        } elseif ($yubico_api_key2 !='' && yubikey_verify_otp($otp,$yubico_api_id,$yubico_api_key2)) {
            return $user;
        } elseif ($yubico_api_key3 !='' && yubikey_verify_otp($otp,$yubico_api_id,$yubico_api_key3)) {
            return $user;
        } else {
            return false;
        }
    }
    return $user;
}

/**
 * User registration.
 * Add Yubikey information to newly created profile.
 * @param user_id
 */

/**
 * Extend personal profile page with Yubikey settings.
 */
function yubikey_profile_personal_options() {
    global $user_id, $is_profile_page;
    $smsauth_user_phone_number = get_user_option('smsauth_phone',$user_id);
    $smsauth_validation = get_user_option('smsauth_validation', $user_id);

    echo "<h3>".__('SMSAuth settings','yubikey')."</h3>";

    echo '<table class="form-table">';
    echo '<tbody>';
    PHP4_Check(false);
    echo '<tr>';
    echo '<th scope="row">'.__('SMSAuth authentication','yubikey').'</th>';
    echo '<td>';

    echo '<div><input name="smsauth_validation" id="smsauth_validation" value="disabled" class="tog" type="radio"';
    if ($smsauth_validation == 'disabled' || $smsauth_validation=='') {
        echo ' checked="checked"';
    }
    echo '/>';
    echo '<label for="smsauth_validation"> '.__('Disabled','yubikey').'</label>';
    echo '</div>';

    echo '<div><input name="smsauth_validation" id="smsauth_validation_on" class="tog" type="radio"';
    if ($smsauth_validation=='on'){
        echo ' checked="checked"';
    }
    echo '/>';
    echo '<label for="smsauth_validation_on"> '.__('Use SMSAuth validation','smsauth').'</label>';
    echo '</div>';

    echo '</td>';
    echo '</tr>';

    if ($is_profile_page || IS_PROFILE_PAGE) {
        echo '<tr>';
        echo '<th><label for="smsauth_phone_no">'.__('Phone Number','smsauth').'</label></th>';
        echo '<td><input name="smsauth_phone_no" id="smsauth_phone_no" value="'.$smsauth_user_phone_number.'" type="text"/><br /></td>';
        echo '</tr>';
    }

    echo '</tbody></table>';
}

/**
 * Extend profile page with ability to enable/disable Yubikey authentication requirement.
 */
function yubikey_edit_user_profile() {
    global $user_id;
    $yubikeyserver=get_user_option('yubikey_server',$user_id);
    $yubikey_key_id=get_user_option('yubikey_key_id',$user_id);

    // Only enable Yubikey settings if user has a key ID attached to profile
    if (strlen($yubikey_key_id)==12) {
        echo "<h3>".__('Yubikey settings','yubikey')."</h3>";

        echo '<table class="form-table">';
        echo '<tbody>';
        PHP4_Check(false);
        echo '<tr>';
        echo '<th scope="row">'.__('Yubikey authentication','yubikey').'</th>';
        echo '<td>';

        echo '<div><input name="yubikey_server" id="yubikeyserver_disabled" value="disabled" class="tog" type="radio"';
        if ($yubikeyserver == 'disabled' || $yubikeyserver=='') {
            echo ' checked="checked"';
        }
        echo '/>';
        echo '<label for="yubikeyserver_disabled"> '.__('Disabled','yubikey').'</label>';
        echo '</div>';

        echo '<div><input name="yubikey_server" id="yubikeyserver_yubico" value="yubico" class="tog" type="radio"';
        if ($yubikeyserver=='yubico'){
            echo ' checked="checked"';
        }
        echo '/>';
        echo '<label for="yubikeyserver_yubico"> '.__('Use Yubico server','yubikey').'</label>';
        echo '</div>';

        echo '</td>';
        echo '</tr>';

        echo '</tbody></table>';
    }
}

function smsauth_personal_options_update() {
    global $user_id;
    $smsauth_validation = trim($_POST['smsauth_validation']);
    $smsauth_phone_no = trim($_POST['smsauth_phone_no']);

    $options = get_option('smsauth_options');
    $service_name = $options['smsauth_service_name'];
    $token = $options['smsauth_service_token'];
    $old_phone_number = get_user_option('smsauth_phone', $user_id);
    if ($old_phone_number !== $smsauth_phone_no) {
        smsauth_add_phone($service_name, $token, $smsauth_phone_no);
        update_user_option($user_id, 'smsauth_phone', $smsauth_phone_no, true);
    }
    update_user_option($user_id, 'smsauth_validation', $smsauth_validation, true);
}

function smsauth_check_validation_code($user) {

    $smsauth_validation = 'on';

    if ($smsauth_validation == 'on') {
        $smsauth_code = $_POST['smsauth'];

        $options = get_option('smsauth_options');
        $service_name = $options['smsauth_service_name'];
        $password = $options['smsauth_service_pass'];
        $token = $options['smsauth_service_token'];

        $smsauth_user_phone_number = "925-271-7005";
        $valid_code = smsauth_verify($service_name, $token, 
                                     $smsauth_user_phone_number, $smsauth_code);

        if ($valid_code) {
            return $user;
        }
        else {
            return false;
        }
    }
    else {
        return $user;
    }
}

/**
 * Verify HMAC-SHA1 signatur on result received from Yubico server
 * @param String $response Data from Yubico
 * @param String $yubico_api_key Shared API key
 * @return Boolean Does the signature match ?
 */
function yubikey_verify_hmac($response,$yubico_api_key) {
    $lines=split("\n",$response);
    // Create array from data
    foreach ($lines as $line) {
        $lineparts=split("=",$line,2);
        $result[$lineparts[0]]=trim($lineparts[1]);
    }
    // Sort array Alphabetically based on keys
    ksort($result);
    // Grab the signature sent by server, and delete
    $signatur=$result['h'];
    unset($result['h']);
    // Build new string to calculate hmac signature on
    $datastring='';
    foreach ($result as $key=>$value) {
        $datastring!='' ? $datastring.="&" : $datastring.='';
        $datastring.=$key."=".$value;
    }
    $hmac = base64_encode(hash_hmac('sha1',$datastring,base64_decode($yubico_api_key), TRUE));
    return $hmac==$signatur;
}

/**
 * Call the Auth API at Yubico server
 *
 * @param String $otp One-time Password entered by user
 * @param String $yubico_id ID at Yubico server
 * @param String $yubico_api_key Shared API key
 * @return Boolean Is the password OK ?
 */
function yubikey_verify_otp($otp,$yubico_api_id,$yubico_api_key){
    $url="http://api.yubico.com/wsapi/verify?id=".$yubico_api_id."&otp=".$otp;

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Wordpress Yubikey OTP login plugin");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = trim(curl_exec($ch));
    curl_close($ch);

    if (yubikey_verify_hmac($response,$yubico_api_key)) {
        if(!preg_match("/status=([a-zA-Z0-9_]+)/", $response, $result)) {
            return false;
        }
        if ($result[1]=='OK') {
            return true;
        }
    }
    return false;
}

// Initialization and Hooks
add_action('personal_options_update','smsauth_personal_options_update');
add_action('profile_personal_options','yubikey_profile_personal_options');

add_action('edit_user_profile','yubikey_edit_user_profile');
add_action('edit_user_profile_update','yubikey_edit_user_profile_update');

add_action('admin_menu','smsauth_admin');

add_action('admin_menu', 'smsauth_admin_add_page');

add_filter('plugin_action_links', 'smsauth_plugin_action_links', 10, 2);

function smsauth_admin_add_page() {
    add_options_page('SMSAuth', 'SMSAuth', 8, 'smsauth', 'smsauth_options_page');
}

function smsauth_options_page() {
?>
<div>
<h2>SMSAuth Service Settings</h2>
<form action="options.php" method="post">
<?php settings_fields('smsauth_options'); ?>
<?php do_settings_sections('smsauth'); ?>
<?php submit_button('Save Changes'); ?>
</form>
</div>
<?php
}

add_action('admin_init', 'smsauth_admin_init');

function smsauth_admin_init() {
    register_setting('smsauth_options', 'smsauth_options', 
        'smsauth_options_validate');
    add_settings_section('smsauth_main', 'Service Registration',
        'smsauth_section_text', 'smsauth');
    add_settings_field('smsauth_service_name', 'SMSAuth Service Name',
        'smsauth_service_name', 'smsauth', 'smsauth_main');
    add_settings_field('smsauth_service_pass', 'SMSAuth Service Password',
        'smsauth_service_pass', 'smsauth', 'smsauth_main');
}

function smsauth_section_text() {
    $options = get_option('smsauth_options');
    if (isset($options['smsauth_service_name'])) {
        echo '<p>To use SMSAuth, you need to register a service.</p>';
    }
}

function smsauth_service_name() {
    $options = get_option('smsauth_options');
    if (isset($options['smsauth_service_name'])) {
        $service_name = $options['smsauth_service_name'];
    } else {
        $service_name = "";
    }
    echo "<input id='service_name' name='smsauth_options[smsauth_service_name]'
        size='40' type='text' value='$service_name' />";
}

function smsauth_service_pass() {
    $options = get_option('smsauth_options');
    if (isset($options['smsauth_service_pass'])) {
        $service_pass = $options['smsauth_service_pass'];
    } else {
        $service_pass = "";
    }
    echo "<input id='service_name' name='smsauth_options[smsauth_service_pass]'
        size='40' type='text' value='$service_pass' />";
}

function smsauth_options_validate($input) {
    global $smsauth_api_url, $smsauth_user_agent;

    $new_service_name = $input['smsauth_service_name'];
    $new_service_pass = $input['smsauth_service_pass'];
    $options = get_option('smsauth_options');

    $url = "$smsauth_api_url/services";
    _log("Making HTTP request to /api/services");

    if (!isset($options['smsauth_service_name']) || 
            $new_service_name !== $options['smsauth_service_name']) {
        /* register a new service and delete the old one. */
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "service_name=" . 
            urlencode($new_service_name) . "&password=" . urlencode($new_service_pass));
        curl_setopt($ch, CURLOPT_USERAGENT, $smsauth_user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        $json_response = json_decode($response);
        if (isset($json_response->token)) {
            $options['smsauth_service_token'] = $json_response->token;
            update_option('smsauth_options', $options);
        }
        curl_close($ch);
    }
    return $input;
}

// Add a settings link to the plugins page
function smsauth_plugin_action_links($action_links, $plugin) {
    // XXX write this if statement in "The Wordpress Way"
    if ($plugin === 'smsauth/smsauth.php') {
        $action_links[] = '<a href="' . admin_url('options-general.php?page=smsauth') .
            '">Settings</a>';
    }
    return $action_links;
}

// If vital functions are missing in the PHP installation we don't enable the
// wp_authenticate_user filter.
if (function_exists('curl_init') && function_exists('hash_hmac')) {
        //add_action('login_form', 'smsauth_login_form');
        //add_filter('wp_authenticate_user','smsauth_check_validation_code');

        // User registration functions
        add_action('user_register','yubikey_user_register');
        add_action('register_form','yubikey_registerform');

} ?>
